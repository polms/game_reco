## Introduction

The problem below requires some kind of input. You are free to implement any mechanism for feeding input into your solution 
(for example, using hard coded data within a unit test). You should provide sufficient evidence that your solution is complete. 
You are free in choosing your language (but you should explain why you have picked it) and your frameworks. 

## Problem: Simple Product Recommendation

A customer plays a game in one of our web shops. Right after the customer has completed the game, a small layer should be shown to the customer with at least three other products that she might be interested in.

You should implement a web service which sends back a given number of product (game) recommendations for a given customer number. The response format should be a json object.

Example request to get 5 recommendations for a given customer number '111111'

```
GET /customers/111111/games/recommendations?count=5
```

You should also provide an endpoint to upload a csv file which contains recommendations for customers.
The csv file has the following format:

```
"CUSTOMER_NUMBER","RECOMMENDATION_ACTIVE","REC1","REC2","REC3", REC4","REC5","REC6","REC7","REC8","REC9","REC10"
```

Example:

```
"CUSTOMER_NUMBER","RECOMMENDATION_ACTIVE","REC1","REC2","REC3", REC4","REC5","REC6","REC7","REC8","REC9","REC10"
"111111","true","bingo","cashwheel","cashbuster","brilliant","citytrio","crossword","sevenwins","sudoku","sofortlotto","hattrick"
"111112","false","brilliant","citytrio","crossword","sevenwins","sudoku","sofortlotto","hattrick","bingo","cashwheel","cashbuster"
```


You can use an in-memory solution to store the uploaded product recommendations. Using a real database is a plus.

When a customer number is not available or the 'recommendation_active' flag is 'false' then the returned status code of the GET request should be 404.

## Result

You can send us your code via mail or by providing a link to an online repository (github, bitbucket, etc).
Your result should contain all source code files and a readme file which explains what the service is doing and how the api looks like. 
It should also contain a small description for developers how to build and run the service. Comments in your source code might help to figure out why you have chosen an implementation. 

### Optional: Docker

Since we are deploying all our services with docker it is an additional plus when a Dockerfile is provided which builds an image with your service in it.

 