#!/bin/sh

# passing environment variables as spring-boot arguments
if [ -n "${PROFILE}" ]; then ARG_PROFILE="--spring.profiles.active=$PROFILE"; fi
if [ -n "${DATASOURCE_URL}" ]; then ARG_DATASOURCE_URL="--spring.datasource.url=$DATASOURCE_URL"; fi
if [ -n "${DATASOURCE_USER}" ]; then ARG_DATASOURCE_USER="--spring.datasource.username=$DATASOURCE_USER"; fi
if [ -n "${DATASOURCE_PWD}" ]; then ARG_DATASOURCE_PWD="--spring.datasource.password=$DATASOURCE_PWD"; fi
if [ -n "${RMI_HOSTNAME}" ]; then
  JMX_ARGS="-Dcom.sun.management.jmxremote=true \
    -Dcom.sun.management.jmxremote.local.only=false \
    -Dcom.sun.management.jmxremote.ssl=false \
    -Dcom.sun.management.jmxremote.authenticate=false \
    -Dcom.sun.management.jmxremote.port=1235 \
    -Dcom.sun.management.jmxremote.rmi.port=1235 \
    -Djava.rmi.server.hostname=${RMI_HOSTNAME}";
fi 

# the java server must become pid=1 to be able to process signals
exec java -server ${JMX_ARGS} -jar ./game-reco.jar ${ARG_PROFILE-} ${ARG_DATASOURCE_URL-} ${ARG_DATASOURCE_USER-} ${ARG_DATASOURCE_PWD-}
