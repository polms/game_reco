# case study solution - RESTful webserver

## technology stack

I have chosen Java & Spring Boot as solution technology stack for a couple of reasons

* open source
* well documented
* stable and proven
* out-of-the-box solutions for the given problem domain
* known by me ;-)

### first step - build the server via maven
```
mvn clean compile
``` 
### second step - start the server
```
java -jar game-reco.jar
``` 
### third step - interact

The server is listening on port 80 for requests.
You can issue GET requests for customer game recommendations.

```
GET /customers/<customer-id\>/games/recommendations[?count=<limit>]
```

The server responds with a JSON object looking like this

```
{"id":111111,"games":["bingo","cashwheel","cashbuster","brilliant","citytrio"]}
```

You can upload customer data from csv-files via post requests
From the command line you can use curl (f.e.)

```
curl -F file=@<path-to-your-csv-file\> http://localhost:8080/services/upload
```

## details - database

I am using the *H2* in-memory database for simplicity. You can easily exchange *H2* with another database by defining a different *datasource*


## details - docker

A Dockerfile is provided to build a docker image for the server. It's based on actual GGS game-servers and provides support
for optional Spring profile parameters and datasource settings not baked into the images.

Calling *docker build* is not part of the this project, as this is usually part of a GitLab CI or Jenkins pipeline build job
 