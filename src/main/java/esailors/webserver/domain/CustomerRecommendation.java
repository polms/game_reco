package esailors.webserver.domain;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Entity JSON projection class for customer recommendations
 */
public class CustomerRecommendation implements Serializable {

    private static final long serialVersionUID = 1L;

    public CustomerRecommendation(Customer customer, int limit) {
    	this.id = customer.getId();
    	this.games = customer.getGames().stream().limit(limit).map(CustomerGame::getGame).collect(Collectors.toList());
    }

    private long id;

    private List<String> games;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<String> getGames() {
		return games;
	}

	public void setGames(List<String> games) {
		this.games = games;
	}
}
