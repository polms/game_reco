package esailors.webserver.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@Table(name = "customer")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private long id;

    @Column(nullable = false, updatable = true)
    private boolean recommendationActive;

    @OneToMany(mappedBy = "customerId", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @BatchSize(size = 10)
    private List<CustomerGame> games;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isRecommendationActive() {
		return recommendationActive;
	}

	public void setRecommendationActive(boolean recommendationActive) {
		this.recommendationActive = recommendationActive;
	}

	public List<CustomerGame> getGames() {
		return games == null ? games = new ArrayList<>() : games;
	}

	public void setGames(List<CustomerGame> games) {
		this.games = games;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", recommendationActive=" + recommendationActive + ", games=" + games + "]";
	}
}
