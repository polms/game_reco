package esailors.webserver.domain;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	public CustomerRecommendation getRecommendationForCustomer(long customerId, int limit) {
		Customer customer = customerRepository.findByIdAndRecommendationActive(customerId, true);
		if (customer != null) {
			return new CustomerRecommendation(customer, limit);
		}
		return null;
	}

    @Transactional
	public int importRecommendations(String csvBuffer) throws IOException {
    	return customerRepository.save(csvToCustomers(csvBuffer)).size();
	}

    List<Customer> csvToCustomers(String csvBuffer) throws IOException {
    	List<Customer> customers = new ArrayList<>();
    	try (CSVParser parser = new CSVParser(new StringReader(csvBuffer), CSVFormat.DEFAULT.withHeader())) {
			for (CSVRecord record : parser) {
				customers.add(csvToCustomer(record));
			}
		}
    	return customers;
	}

    Customer csvToCustomer(CSVRecord record) {
		Customer customer = new Customer();
		customer.setId(Long.parseLong(record.get("CUSTOMER_NUMBER")));
		customer.setRecommendationActive(Boolean.parseBoolean(record.get("RECOMMENDATION_ACTIVE")));
		for (int i = 1; i <= 10; ++i) {
			CustomerGame game = new CustomerGame();
			game.setCustomerId(customer.getId());
			game.setGame(record.get("REC" + i));
			customer.getGames().add(game);
		}
		return customer;
    }
}
