package esailors.webserver.controller;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import esailors.webserver.domain.CustomerRecommendation;
import esailors.webserver.domain.CustomerService;

@RestController
public class GameRecommendationController  {

    private CustomerService customerService;

	GameRecommendationController(CustomerService customerService) {
		this.customerService = customerService;
	}

	@GetMapping(path = "/customers/{customerId}/games/recommendations")
	ResponseEntity<?> recommendations(@PathVariable long customerId, @RequestParam(value = "count", defaultValue = "5") int limit) {
		CustomerRecommendation response = customerService.getRecommendationForCustomer(customerId, limit);
		if (response != null) {
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
	}

	// Single file customer recommendation data upload
    @PostMapping("/services/upload")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return new ResponseEntity<>("please select a file!", HttpStatus.BAD_REQUEST);
        }
        try {
            int count = customerService.importRecommendations(new String(file.getBytes()));
            return new ResponseEntity<>("successfully imported " + count + " recommendations from: " + file.getOriginalFilename(), HttpStatus.OK);
        } catch (IOException ioe) {
            return new ResponseEntity<>("failed importing from : " + file.getOriginalFilename(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
