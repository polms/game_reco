package esailors.webserver.domain;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CustomerServiceTest {

	@MockBean
	CustomerRepository customerRepository;

	CustomerService customerService;

	@Before
	public void setup() {
		customerService = new CustomerService(customerRepository);
	}

	@Test
	public void csvToCustomers() throws Exception {

		String csv = "\"CUSTOMER_NUMBER\",\"RECOMMENDATION_ACTIVE\",\"REC1\",\"REC2\",\"REC3\",\"REC4\",\"REC5\",\"REC6\",\"REC7\",\"REC8\",\"REC9\",\"REC10\"\n" +
				"\"1\",\"true\",\"game_1_1\",\"game_1_2\",\"game_1_3\",\"game_1_4\",\"game_1_5\",\"game_1_6\",\"game_1_7\",\"game_1_8\",\"game_1_9\",\"game_1_10\"\n" +
				"\"2\",\"false\",\"game_2_1\",\"game_2_2\",\"game_2_3\",\"game_2_4\",\"game_2_5\",\"game_2_6\",\"game_2_7\",\"game_2_8\",\"game_2_9\",\"game_2_10\"\n";

		List<Customer> customers = customerService.csvToCustomers(csv);

		Assert.assertEquals(2, customers.size());

		Assert.assertEquals(1, customers.get(0).getId());
		Assert.assertEquals(true, customers.get(0).isRecommendationActive());
		Assert.assertEquals(10, customers.get(0).getGames().size());
		Assert.assertEquals(1, customers.get(0).getGames().get(2).getCustomerId());
		Assert.assertEquals("game_1_3", customers.get(0).getGames().get(2).getGame());

		Assert.assertEquals(2, customers.get(1).getId());
		Assert.assertEquals(false, customers.get(1).isRecommendationActive());
		Assert.assertEquals(10, customers.get(1).getGames().size());
		Assert.assertEquals(2, customers.get(1).getGames().get(9).getCustomerId());
		Assert.assertEquals("game_2_10", customers.get(1).getGames().get(9).getGame());
	}
}
