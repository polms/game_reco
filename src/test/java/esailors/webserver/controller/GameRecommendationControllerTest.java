package esailors.webserver.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import esailors.webserver.domain.Customer;
import esailors.webserver.domain.CustomerGame;
import esailors.webserver.domain.CustomerRecommendation;
import esailors.webserver.domain.CustomerService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = GameRecommendationController.class, secure = false)
public class GameRecommendationControllerTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	CustomerService customerService;

	Customer activeCustomer;

	@Before
	public void setup() {
		activeCustomer = createCustomer(1, true);
	}

	Customer createCustomer(long id, boolean active) {
		Customer customer = new Customer();
		customer.setId(id);
		customer.setRecommendationActive(active);
		for (int i = 1; i <= 10; ++i) {
			CustomerGame game = new CustomerGame();
			game.setId(i);
			game.setCustomerId(id);
			game.setGame("game_" + id + "_" + i);
			customer.getGames().add(game);
		}
		return customer;
	}

	MvcResult performGetRequest(String url) throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(url).accept(MediaType.APPLICATION_JSON);
		return mockMvc.perform(requestBuilder).andReturn();
	}

	@Test
	public void getActiveCustomerWithoutLimit() throws Exception {

		CustomerRecommendation recommendation = new CustomerRecommendation(activeCustomer, 5);
		Mockito.when(customerService.getRecommendationForCustomer(1, 5)).thenReturn(recommendation);

		MvcResult result = performGetRequest("/customers/1/games/recommendations");

		String expected = "{\"id\":1,\"games\":[\"game_1_1\",\"game_1_2\",\"game_1_3\",\"game_1_4\",\"game_1_5\"]}";

		Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

	@Test
	public void getActiveCustomerWithLimit2() throws Exception {

		CustomerRecommendation recommendation = new CustomerRecommendation(activeCustomer, 2);
		Mockito.when(customerService.getRecommendationForCustomer(1, 2)).thenReturn(recommendation);

		MvcResult result = performGetRequest("/customers/1/games/recommendations?count=2");

		String expected = "{\"id\":1,\"games\":[\"game_1_1\",\"game_1_2\"]}";

		Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

	@Test
	public void getUnknownCustomer() throws Exception {

		Mockito.when(customerService.getRecommendationForCustomer(42, 5)).thenReturn(null);

		MvcResult result = performGetRequest("/customers/42/games/recommendations");

		Assert.assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
	}

	@Test
	public void getInactiveCustomer() throws Exception {

		Mockito.when(customerService.getRecommendationForCustomer(2, 5)).thenReturn(null);

		MvcResult result = performGetRequest("/customers/2/games/recommendations");

		Assert.assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
	}
}
